import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Foodgui {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton gyozaButton;
    private JButton karaageButton;
    private JTextField totalTextField;
    private JButton cancelButton;
    private JButton checkButton;
    private JTextPane orderditemstextPane;
    private JTextPane totaltextPane;

    int sum = 0;
    void order(String food,int total) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,"Order for "+ food + " received.");
            String currentText = orderditemstextPane.getText();
            orderditemstextPane.setText(currentText + food +" "+total +"yen"+"\n");
            sum +=total;
            totaltextPane.setText("￥"+sum);
        }
    return;
    }

    public Foodgui(){
        checkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmationfin = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmationfin == 0){
                    String finalvaule = totaltextPane.getText();
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+finalvaule+" yen");
                    orderditemstextPane.setText(null);
                    totaltextPane.setText(null);
                }
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmationfin = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmationfin == 0){
                    orderditemstextPane.setText(null);
                    totaltextPane.setText(null);
                    sum = 0;
                }
            }
        });




        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",100);
                }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.jpg")));



        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",200);
                }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));


        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",300);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba",400);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(this.getClass().getResource("yakisoba.jpg")));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",500);
            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("gyoza.jpg")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",600);
            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg")));
    }




    public static void main(String[] args) {
        JFrame frame = new JFrame("Foodgui");
        frame.setContentPane(new Foodgui().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
